package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MonexMonexUserServiceTest {

    @Mock
    private MonexUserRepository repository;

    private MonexUserService sut;

    private PasswordEncoder passwordEncoder;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        sut = new MonexUserService(repository, new BCryptPasswordEncoder());
    }

    @Test
    void whenSaveNewUser_shouldCallRepository_andReturnSame() {
        MonexUser monexUserToSave = new MonexUser("Name", "Password", "email");
        doReturn(monexUserToSave).when(repository).save(monexUserToSave);

        MonexUser result = sut.save(monexUserToSave);

        verify(repository, times(1)).save(monexUserToSave);
        assertEquals(monexUserToSave, result);
    }
}