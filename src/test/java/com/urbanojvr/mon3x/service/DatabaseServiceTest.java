package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.Expense;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class DatabaseServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private ExpenseRepository expenseRepository;

    private DatabaseService sut;

    @BeforeEach
    void init () {
        MockitoAnnotations.initMocks(this);
        sut = new DatabaseService(categoryRepository, expenseRepository);
    }

    @Test
    void whenPopulate_shouldCallRepositories() {
        ArrayList<Category> categories = new ArrayList<>();
        Category category = new Category(100L, "Name", "Description");
        categories.add(category);

        ArrayList<Expense> expenses = new ArrayList<>();
        Expense expense = new Expense(BigDecimal.valueOf(100.99), new Date(), "Concept", category);
        expenses.add(expense);

        when(categoryRepository.findAll()).thenReturn(categories);
        when(expenseRepository.findAll()).thenReturn(expenses);

        sut.populate();

        verify(categoryRepository, times(sut.getCategoriesToInsert())).save(any());
        verify(expenseRepository, times(sut.getExpensesToInsert())).save(any());
    }
}