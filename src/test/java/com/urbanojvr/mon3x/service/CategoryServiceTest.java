package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CategoryServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private MonexUserRepository monexUserRepository;

    @Mock
    private Authentication authentication;

    @Mock
    private MonexUser monexUser;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private Pageable pageable;

    private CategoryService sut;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
        sut = new CategoryService(monexUserRepository, categoryRepository);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user");
        when(monexUserRepository.findByUsername(anyString())).thenReturn(monexUser);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void whenGetAll_shouldCallRepository_andReturnCorrectList() {
        Page<Category> expected = TestUtils.generateCategoriesPage(5);
        when(categoryRepository.findAllByOwnerId(anyLong(), any(Pageable.class))).thenReturn(expected);

        Page<Category> result = sut.getAll(pageable);

        verify(categoryRepository, times(1)).findAllByOwnerId(anyLong(), any(Pageable.class));
        assertEquals(expected, result);
    }

    @Test
    public void whenGetById_shouldCallRepository_andReturnCorrectObject() throws MonexNotFoundException {
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(expected));

        Category result = sut.getById(1L);

        verify(categoryRepository, times(1)).findById(1L);
        assertEquals(result, expected);
    }

    @Test
    public void whenGetById_ifNotExists_shouldThrowException() {
        Optional<Category> opt = Optional.empty();
        when(categoryRepository.findById(1L)).thenReturn(opt);

        assertThrows(MonexNotFoundException.class, () -> sut.getById(1L));
    }

    @Test
    void whenSaveNewCategory_shouldCallRepository(){
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        CategoryDTO dto = mock(CategoryDTO.class);
        when(categoryRepository.save(expected)).thenReturn(expected);
        when(dto.toCategory()).thenReturn(expected);

        Category result = sut.save(dto);

        verify(categoryRepository, times(1)).save(expected);
        assertEquals(result, expected);
    }

    @Test
    public void whenUpdate_shouldCallRepository() throws MonexNotFoundException {
        ArrayList<Category> categories = TestUtils.generateCategoriesList(2);
        Category old = categories.get(0);
        Category newCategory = categories.get(1);
        CategoryDTO newDto = newCategory.toCategoryDTO();
        when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(old));
        when(categoryRepository.save(any(Category.class))).thenReturn(newCategory);

        Category result = sut.update(1L, newDto);

        verify(categoryRepository, times(1)).save(any(Category.class));
        assertEquals(result, newCategory);
    }

    @Test
    public void whenUpdate_ifNotExists_thenThrowException() {
        when(categoryRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(MonexNotFoundException.class, () -> sut.update(1L, mock(CategoryDTO.class)));
    }
    
    @Test
    void whenDelete_shouldCallRepository() {
        sut.delete(1L);

        verify(categoryRepository, times(1)).deleteById(1L);
    }
}
