package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExpenseServiceTest {

    @Mock
    private ExpenseRepository expenseRepository;

    @Mock
    private MonexUserRepository monexUserRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private Authentication authentication;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private MonexUser monexUser;

    @InjectMocks
    private ExpenseService sut;

    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
        sut = new ExpenseService(monexUserRepository, expenseRepository);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user");
        when(monexUserRepository.findByUsername(anyString())).thenReturn(monexUser);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    void whenGetAll_shouldCallRepositoryAndReturnSameList() {
        Pageable pageable = mock(Pageable.class);
        Page<Expense> expected = TestUtils.generateExpensesPage(5);
        when(expenseRepository.findAllByOwnerId(1L, pageable)).thenReturn(expected);
    }

    @Test
    void whenSave_shouldCallRepository_andReturnSameObject() {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseRepository.save(any(Expense.class))).thenReturn(expected);

        Expense result = sut.save(dto);

        verify(expenseRepository, times(1)).save(any(Expense.class));
        assertEquals(result, expected);
    }

    @Test
    void whenUpdate_shouldCallRepository() throws MonexNotFoundException {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseRepository.findById(1L)).thenReturn(Optional.of(expected));
        when(expenseRepository.save(expected)).thenReturn(expected);

        Expense result = sut.update(1L, dto);

        verify(expenseRepository, times(1)).save(expected);
        assertEquals(result, expected);
    }

    @Test
    void whenDelete_shouldCallRepository() {
        Expense expenseToSave = TestUtils.generateExpensesList(1).get(0);
        expenseToSave.getCategory().setOwner(monexUser);
        expenseToSave.setOwner(monexUser);
        Optional<Category> categoryOpt = Optional.of(expenseToSave.getCategory());
        Optional<Expense> expenseOpt = Optional.of(expenseToSave);

        when(categoryRepository.findById(anyLong())).thenReturn(categoryOpt);
        when(expenseRepository.findById(anyLong())).thenReturn(expenseOpt);
        when(expenseRepository.existsById(anyLong())).thenReturn(true);

        sut.delete(anyLong());

        verify(expenseRepository, times(1)).deleteById(anyLong());
    }

    @Test
    void whenGetMyYearAndMonth_shouldCallRepository_andReturnCorrectList() {
        ArrayList<Expense> expected = TestUtils.generateExpensesList(5);
        doReturn(expected).when(expenseRepository).findAllByOwnerAndDateGreaterThanEqualAndDateLessThanEqualOrderByDate(any(monexUser.getClass()), any(Date.class), any(Date.class));

        ArrayList<Expense> result = (ArrayList<Expense>) sut.getByYearAndMonth(2020, 10);

        verify(expenseRepository, times(1)).findAllByOwnerAndDateGreaterThanEqualAndDateLessThanEqualOrderByDate(any(monexUser.getClass()), any(Date.class), any(Date.class));
        assertEquals(expected, result);
    }
}