package com.urbanojvr.mon3x.authorization;

import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CategoryAuthorizationTest {

    @Mock
    private CategoryService categoryService;

    @Mock
    private Pageable pageable;

    @InjectMocks
    private CategoryAuthorization sut;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.sut = new CategoryAuthorization(this.categoryService);
    }

    @Test
    public void whenGetAll_shouldCallService_andReturnCorrectList() {
        Page<Category> expected = TestUtils.generateCategoriesPage(5);
        when(categoryService.getAll(any(Pageable.class))).thenReturn(expected);

        Page<Category> result = sut.getAll(pageable);

        verify(categoryService, times(1)).getAll(any(Pageable.class));
        assertEquals(result, expected);
    }

    @Test
    public void whenGetById_shouldCallService_andReturnCorrectObject() throws MonexUnauthorizedException, MonexNotFoundException {
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryService.getById(1L)).thenReturn(expected);
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(true);

        Category result = sut.getById(1L);

        verify(categoryService, times(1)).getById(1L);
        assertEquals(result, expected);
    }

    @Test
    public void whenGetById_ifLoggedUserIsNotTheOwner_shouldThrowsException() {
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(false);

        assertThrows(MonexUnauthorizedException.class, () -> sut.getById(1L));
    }

    @Test
    public void whenSave_shouldCallService() {
        CategoryDTO dto = mock(CategoryDTO.class);
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryService.save(dto)).thenReturn(expected);

        Category result = sut.save(dto);

        verify(categoryService, times(1)).save(dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenUpdate_shouldCallService() throws MonexNotFoundException, MonexUnauthorizedException {
        CategoryDTO dto = mock(CategoryDTO.class);
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(true);
        when(categoryService.update(1L, dto)).thenReturn(expected);

        Category result = sut.update(1L, dto);

        verify(categoryService, times(1)).update(1L, dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenUpdate_ifUserNotOwner_thenThrowException() {
        CategoryDTO dto = mock(CategoryDTO.class);
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(false);

        assertThrows(MonexUnauthorizedException.class, () -> sut.update(1L, dto));
    }

    @Test
    public void whenDelete_thenCallService() throws MonexUnauthorizedException {
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(true);

        sut.delete(1L);

        verify(categoryService, times(1)).delete(1L);
    }

    @Test
    public void whenDelete_ifUserNotOwner_thenThrowException() {
        when(categoryService.loggedUserIsOwner(1L)).thenReturn(false);

        assertThrows(MonexUnauthorizedException.class, () -> sut.delete(1L));
    }
}