package com.urbanojvr.mon3x.authorization;

import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import com.urbanojvr.mon3x.service.ExpenseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExpenseAuthorizationTest {

    @Mock
    private ExpenseService expenseService;

    @Mock
    private CategoryService categoryservice;

    @InjectMocks
    private ExpenseAuthorization sut;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.sut = Mockito.spy(new ExpenseAuthorization(expenseService, categoryservice));
    }

    @Test
    public void whenGetAll_shouldCallserviceAndReturnCorrectList() {
        Pageable pageable = mock(Pageable.class);
        Page<Expense> expected = TestUtils.generateExpensesPage(5);
        when(expenseService.getAll(pageable)).thenReturn(expected);

        Page<Expense> result = sut.getAll(pageable);

        verify(expenseService, times(1)).getAll(pageable);
        assertEquals(result, expected);
    }

    @Test
    public void whenSaveSuccess_shouldCallService_andReturnSavedObject() throws MonexUnauthorizedException, MonexNotFoundException {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseService.save(dto)).thenReturn(expected);
        doReturn(true).when(sut).verifyCategoryOwner(anyLong());

        Expense result = sut.save(dto);

        verify(expenseService, times(1)).save(dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenSave_ifLoggedUserNotOwnerOfCategory_thenReturnException() throws MonexNotFoundException {
        ExpenseDTO dto = TestUtils.generateExpensesList(1).get(0).toDto();
        doReturn(false).when(sut).verifyCategoryOwner(anyLong());

        assertThrows(MonexUnauthorizedException.class, () -> sut.save(dto));
    }

    @Test
    public void whenDelete_success() throws MonexUnauthorizedException {
        when(expenseService.loggedUserIsOwner(1L)).thenReturn(true);

        sut.delete(1L);

        verify(expenseService, times(1)).delete(1L);
    }

    @Test
    public void whenDelete_ifUserNotOwner_shouldThrowException() {
        when(expenseService.loggedUserIsOwner(1L)).thenReturn(false);

        assertThrows(MonexUnauthorizedException.class, () -> sut.delete(1L));
    }

    @Test
    public void whenUpdateSuccess_shouldCallService_andReturnUpdatedObject() throws MonexNotFoundException, MonexUnauthorizedException {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseService.update(1L, dto)).thenReturn(expected);
        when(expenseService.loggedUserIsOwner(anyLong())).thenReturn(true);

        Expense result = sut.update(1L, dto);

        verify(expenseService, times(1)).update(1L, dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenUpdate_ifUserIsNotExpenseOwnerOrNotCategoryOwner_shouldThrowException() throws MonexNotFoundException {
        Expense expense = TestUtils.generateExpensesList(1).get(0);
        doReturn(false).when(sut).verifyCategoryOwner(anyLong());

        assertThrows(MonexUnauthorizedException.class, () -> sut.update(1L, expense.toDto()));
    }

    @Test
    public void whenVerifyCategoryOwner_success() throws MonexNotFoundException {
        MonexUser user = mock(MonexUser.class);
        when(user.getId()).thenReturn(0L);
        Category category = TestUtils.generateCategoriesList(1).get(0);
        category.setOwner(user);
        when(categoryservice.getById(anyLong())).thenReturn(category);
        when(expenseService.getLoggedUserId()).thenReturn(0L);

        assertTrue(sut.verifyCategoryOwner(1));

        verify(categoryservice, times(1)).getById(anyLong());
        verify(expenseService, times(1)).getLoggedUserId();
    }

    @Test
    public void whenVerifyCategoryOwner_fail() throws MonexNotFoundException {
        MonexUser user = mock(MonexUser.class);
        when(user.getId()).thenReturn(0L);
        Category category = TestUtils.generateCategoriesList(1).get(0);
        category.setOwner(user);
        when(categoryservice.getById(anyLong())).thenReturn(category);
        when(expenseService.getLoggedUserId()).thenReturn(1L);

        assertFalse(sut.verifyCategoryOwner(1));

        verify(categoryservice, times(1)).getById(anyLong());
        verify(expenseService, times(1)).getLoggedUserId();
    }
}