package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.service.MonexUserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class MonexUserControllerTest {

    @Mock
    private MonexUserService service;

    private UserController sut;

    @BeforeEach
    private void init() {
        MockitoAnnotations.initMocks(this);
        this.sut = new UserController(service);
    }

    @Test
    void whenSaveUser_shouldCallService_andSaveIt() {
        MonexUser monexUserToSave = new MonexUser("UserName", "Password", "email");
        doReturn(monexUserToSave).when(service).save(monexUserToSave);

        MonexUser result = sut.saveUser(monexUserToSave);

        verify(service, times(1)).save(monexUserToSave);
        assertEquals(monexUserToSave, result);
    }
}