package com.urbanojvr.mon3x.controller;

import org.junit.jupiter.api.Test;
import org.springframework.web.bind.annotation.ResponseBody;

import static org.junit.jupiter.api.Assertions.*;

class HelloTest {

    @ResponseBody
    public String sayHello() {
        return "Hello";
    }
}
