package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.service.DatabaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class DatabaseControllerTest {

    @Mock
    private DatabaseService service;

    private DatabaseController sut;

    @BeforeEach
    void init () {
        MockitoAnnotations.initMocks(this);
        sut = new DatabaseController(service);
    }

    @Test
    void whenPopulate_shouldCallService() {
        sut.populateDatabase();

        verify(service, times(1)).populate();
    }
}