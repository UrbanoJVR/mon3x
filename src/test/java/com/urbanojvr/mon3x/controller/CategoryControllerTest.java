package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.authorization.CategoryAuthorization;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class CategoryControllerTest {

    @Mock
    private CategoryService categoryService;

    @Mock
    private CategoryAuthorization categoryAuthorization;

    @Mock
    private Pageable pageable;

    @InjectMocks
    private CategoryController sut;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.sut = new CategoryController(categoryService, categoryAuthorization);
    }

    @Test
    public void whenGetAll_shouldCallAuthorizerAndReturnTheCorrectList() {
        Page<Category> expected = TestUtils.generateCategoriesPage(5);
        when(categoryAuthorization.getAll(pageable)).thenReturn(expected);

        Page<Category> result = sut.getAll(pageable);

        verify(categoryAuthorization, times(1)).getAll(pageable);
        assertEquals(result, expected);
    }

    @Test
    public void whenGetById_thenCallAuthorizer_andReturnCorrectObject() throws MonexUnauthorizedException, MonexNotFoundException {
        Category expected = TestUtils.generateCategoriesList(1).get(0);

        when(categoryAuthorization.getById(expected.getId())).thenReturn(expected);

        Category result = sut.getById(expected.getId());

        verify(categoryAuthorization, times(1)).getById(expected.getId());
        assertEquals(result, expected);
    }

    @Test
    public void whenGetById_ifUserIsNotOwner_shouldThrowException() throws MonexUnauthorizedException, MonexNotFoundException {
        when(categoryAuthorization.getById(1L)).thenThrow(MonexUnauthorizedException.class);

        assertThrows(MonexUnauthorizedException.class, () -> sut.getById(1L));
    }

    @Test
    public void whenGetbyId_ifCategoryNotExists_shouldThrowException() throws MonexUnauthorizedException, MonexNotFoundException {
        when(categoryAuthorization.getById(1L)).thenThrow(MonexNotFoundException.class);

        assertThrows(MonexNotFoundException.class, () -> sut.getById(1L));
    }

    @Test
    public void whenSave_shouldCallAuthorizer() {
        CategoryDTO dto = mock(CategoryDTO.class);
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryAuthorization.save(dto)).thenReturn(expected);

        Category result = sut.save(dto);

        verify(categoryAuthorization, times(1)).save(dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenUpdate_ShouldCallAuthorizer() throws MonexNotFoundException, MonexUnauthorizedException {
        CategoryDTO dto = mock(CategoryDTO.class);
        Category expected = TestUtils.generateCategoriesList(1).get(0);
        when(categoryAuthorization.update(1L, dto)).thenReturn(expected);

        Category result = sut.update(1L, dto);

        verify(categoryAuthorization, times(1)).update(1L, dto);
        assertEquals(result, expected);
    }

    @Test
    public void whenDelete_thenCallAuthorizer() throws MonexUnauthorizedException {
        sut.delete(1L);

        verify(categoryAuthorization, times(1)).delete(1L);
    }
}
