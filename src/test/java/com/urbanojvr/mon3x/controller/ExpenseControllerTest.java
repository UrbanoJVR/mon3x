package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.TestUtils;
import com.urbanojvr.mon3x.authorization.ExpenseAuthorization;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.ExpenseService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Field;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ExpenseControllerTest {

    @Mock
    private ExpenseService service;

    @Mock
    private ExpenseAuthorization expenseAuthorization;

    private ExpenseController sut;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        this.sut = new ExpenseController(service, expenseAuthorization);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void whenGetAllExpenses_shouldCallAuthorizerAndReturnCorrectList() {
        Pageable pageable = mock(Pageable.class);
        Page<Expense> expected = TestUtils.generateExpensesPage(5);
        doReturn(expected).when(expenseAuthorization).getAll(pageable);

        Page<Expense> result = sut.getAll(pageable);

        verify(expenseAuthorization, times(1)).getAll(pageable);
        assertEquals(expected, result);
    }

    @Test
    void whenSaveNewExpense_shouldCallAuthorizer_andReturnSavedObject() throws MonexUnauthorizedException, MonexNotFoundException {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseAuthorization.save(dto)).thenReturn(expected);

        Expense result = sut.save(dto);

        verify(expenseAuthorization, times(1)).save(dto);
        assertEquals(result, expected);
    }

    @Test
    void whenDeleteExpense_shouldCallAuthorizer() throws MonexUnauthorizedException {
        sut.delete(1L);

        verify(expenseAuthorization, times(1)).delete(1L);
    }

    @Test
    public void whenUpdate_shouldCallAuthorizer_andReturnUpdatedObject() throws MonexNotFoundException, MonexUnauthorizedException {
        Expense expected = TestUtils.generateExpensesList(1).get(0);
        ExpenseDTO dto = expected.toDto();
        when(expenseAuthorization.update(1L, dto)).thenReturn(expected);

        Expense result = sut.update(1L, dto);

        verify(expenseAuthorization, times(1)).update(1L, dto);
        assertEquals(result, expected);
    }

    @Test
    void whenGetMyMonth_shouldCallService_andReturnCorrectList() {
        ArrayList<Expense> expected = TestUtils.generateExpensesList(5);
        doReturn(expected).when(service).getByYearAndMonth(anyInt(), anyInt());

//        ArrayList<Expense> result = (ArrayList<Expense>) sut.getByYearAndMonth(anyInt(), anyInt());
//        assertEquals(expected, result);
    }

}