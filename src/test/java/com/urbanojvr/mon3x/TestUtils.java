package com.urbanojvr.mon3x;

import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.urbanojvr.mon3x.config.Constants.HEADER_AUTHORIZATION_KEY;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class TestUtils {

    public static final String USER_NAME = "user";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";

    public static void insertUser(PasswordEncoder passwordEncoder, MonexUserRepository monexUserRepository) {
        if(monexUserRepository.findAll().size() == 0) {
            MonexUser monexUser = new MonexUser(USER_NAME, PASSWORD, EMAIL);
            monexUser.setPassword(passwordEncoder.encode(monexUser.getPassword()));

            monexUserRepository.save(monexUser);
        }
    }

    public static String obtainToken(MockMvc mvc) throws Exception {
        String json = "{\"name\":\"" + USER_NAME + "\", \"password\":\"" + PASSWORD + "\"}";

        ResultActions result = mvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON).content(json));

        return result.andReturn().getResponse().getHeader(HEADER_AUTHORIZATION_KEY);
    }

    public static Page<Category> generateCategoriesPage(int wantedSize) {
        return new PageImpl<>(generateCategoriesList(wantedSize));
    }

    public static ArrayList<Category> generateCategoriesList(int wantedSize) {
        ArrayList<Category> categories = new ArrayList<>();

        for (int i = 0; i < wantedSize; i ++){
            Category category = new Category(i, "Category " + i, "Description " + i);
            categories.add(category);
        }

        return categories;
    }

    public static Page<Expense> generateExpensesPage(int wantedSize) {
        return new PageImpl<>(generateExpensesList(wantedSize));
    }

    public static ArrayList<Expense> generateExpensesList(int wantedSize) {
        ArrayList<Expense> expenses = new ArrayList<>();

        for(int i = 0; i < wantedSize; i ++) {
            Expense expense = new Expense(BigDecimal.valueOf(200.00F + i), new Date(), "Concept " + i,
                    new Category("Category " + i, "Description " + i));
            expenses.add(expense);
        }

        return expenses;
    }
}
