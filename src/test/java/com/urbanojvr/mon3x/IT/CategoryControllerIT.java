package com.urbanojvr.mon3x.IT;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.util.ArrayList;

import static com.urbanojvr.mon3x.TestUtils.*;
import static com.urbanojvr.mon3x.config.Constants.HEADER_AUTHORIZATION_KEY;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class CategoryControllerIT {

    private String token;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MonexUserRepository monexUserRepository;

    @BeforeEach
    void prepare() throws Exception {
        insertUser(passwordEncoder, monexUserRepository);
        token = obtainToken(mvc);
        insertCategory();
    }

    @AfterEach
    void cleanDatabase() {
//        userRepository.deleteAll();
//        categoryRepository.deleteAll();
    }

    @Test
    void givenCategories_whenGetAll_thenStatus200() throws Exception {
        mvc.perform(get("/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .header(HEADER_AUTHORIZATION_KEY, token))
                .andExpect(status().isOk());
    }

    @Test
    void givenCategory_whenDelete_thenStatus200() throws Exception {
        ArrayList<Category> list = categoryRepository.findAll();
        long id = list.get(0).getId();

        mvc.perform(delete("/categories/" + id)
                .header(HEADER_AUTHORIZATION_KEY, token))
                .andExpect(status().isOk());
    }

    @Test
    void givenCategory_whenUpdate_thenStatus200() throws Exception{
        long existingId = categoryRepository.findAll().get(0).getId();
        Category c = new Category(existingId, "New Name", "New Description");
        c.setOwner(monexUserRepository.findAll().get(0));
        String json = objectMapper.writeValueAsString(c);

        mvc.perform(put("/categories")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    @Test
    void whenPost_thenStatus200() throws Exception {
        Category c = new Category("New Name", "New Description");
        c.setOwner(monexUserRepository.findAll().get(0));
        String json = objectMapper.writeValueAsString(c);

        mvc.perform(post("/categories")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());
    }

    private void insertCategory(){
        Category c = new Category("Name", "Description");
        MonexUser monexUser = monexUserRepository.findAll().get(0);
        c.setOwner(monexUser);

        categoryRepository.save(c);
    }
}
