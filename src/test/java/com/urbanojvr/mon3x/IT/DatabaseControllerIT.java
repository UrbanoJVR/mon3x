package com.urbanojvr.mon3x.IT;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.service.DatabaseService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class DatabaseControllerIT {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ExpenseRepository expenseRepository;

    private DatabaseService databaseService;

    @BeforeEach
    void prepare() {
        databaseService = new DatabaseService(categoryRepository, expenseRepository);
    }

    @AfterEach
    void cleanDatabase() {
        expenseRepository.deleteAll();
        categoryRepository.deleteAll();
    }

//    @Test
//    void whenPopulate_thenStatus200_andShouldInsertData() throws Exception {
//        mvc.perform(get("/database/populate"));
//
//        int categories = categoryRepository.findAll().size();
//        int expenses = expenseRepository.findAll().size();
//
//        assertEquals(categories, databaseService.getCategoriesToInsert());
//        assertEquals(expenses, databaseService.getExpensesToInsert());
//    }
}
