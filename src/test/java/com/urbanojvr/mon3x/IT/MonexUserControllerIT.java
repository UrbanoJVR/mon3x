package com.urbanojvr.mon3x.IT;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static com.urbanojvr.mon3x.TestUtils.insertUser;
import static com.urbanojvr.mon3x.TestUtils.obtainToken;
import static com.urbanojvr.mon3x.config.Constants.HEADER_AUTHORIZATION_KEY;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class MonexUserControllerIT {

    private String token;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Autowired
    private MonexUserRepository monexUserRepository;

    @BeforeEach
    public void prepare() throws Exception {
        insertUser(passwordEncoder, monexUserRepository);
        token = obtainToken(mvc);
    }

    @AfterEach
    public void cleanDatabase() {
        monexUserRepository.deleteAll();
    }

    @Test
    void whenPost_thenStatus200_andShouldSaveIt() throws Exception {
        int usersBefore = monexUserRepository.findAll().size();
        MonexUser monexUser = new MonexUser("Name", "Password", "otherEmail");
        String json = objectMapper.writeValueAsString(monexUser);

        mvc.perform(post("/users")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());

        int usersAfter = monexUserRepository.findAll().size();
        assertEquals(usersBefore, usersAfter - 1);
    }
}
