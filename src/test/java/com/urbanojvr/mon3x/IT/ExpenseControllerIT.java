package com.urbanojvr.mon3x.IT;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

import static com.urbanojvr.mon3x.TestUtils.insertUser;
import static com.urbanojvr.mon3x.TestUtils.obtainToken;
import static com.urbanojvr.mon3x.config.Constants.HEADER_AUTHORIZATION_KEY;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class ExpenseControllerIT {

    private String token;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ExpenseRepository expenseRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MonexUserRepository monexUserRepository;

//    @BeforeTransaction
//    public void prepare() {
//        prepareDatabase();
//    }
//
//    @AfterTransaction
//    void cleanDatabase() {
//        expenseRepository.deleteAll();
//    }

    @BeforeEach
    void prepareDatabaseAndObtainAccessToken() throws Exception {
        insertUser(passwordEncoder, monexUserRepository);
        token = obtainToken(mvc);
        prepareDatabase();
    }

    @AfterEach
    void cleanDatabase() {
        monexUserRepository.deleteAll();
        expenseRepository.deleteAll();
        categoryRepository.deleteAll();
    }

    @Test
    void givenExpenses_whenGetAll_thenStatus200() throws Exception {
        int expectedSize = expenseRepository.findAll().size();

        mvc.perform(get("/expenses")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(expectedSize)));
    }

    @Test
    void whenPost_thenStatus200_andShouldSaveIt() throws Exception {
        int expensesBefore = expenseRepository.findAll().size();
        Category c = categoryRepository.findAll().get(0);
        Expense e = new Expense(BigDecimal.valueOf(100.00F), new Date(), "Concept", c);
        String json = objectMapper.writeValueAsString(e);

        mvc.perform(post("/expenses")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());

        int expensesAfter = expenseRepository.findAll().size();
        assertEquals(expensesBefore, expensesAfter - 1);
    }

    @Test
    void givenExpenses_whenUpdate_thenStatus200_andUpdateIt() throws Exception {
        int expensesBefore = expenseRepository.findAll().size();
        Expense expenseToUpdate = expenseRepository.findAll().get(0);
        expenseToUpdate.setOwner(monexUserRepository.findAll().get(0));
        String oldConcept = expenseToUpdate.getConcept();
        Expense expenseToInsert = new Expense(expenseToUpdate.getId(), BigDecimal.valueOf(20.50F), new Date(), "Other concept", expenseToUpdate.getCategory());
        expenseToInsert.setOwner(monexUserRepository.findAll().get(0));
        String json = objectMapper.writeValueAsString(expenseToInsert);

        mvc.perform(put("/expenses")
                .header(HEADER_AUTHORIZATION_KEY, token)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk());

        Optional<Expense> expenseAfter = expenseRepository.findById(expenseToInsert.getId());
        int expensesAfter = expenseRepository.findAll().size();

        assertEquals(expensesBefore, expensesAfter);
        //noinspection OptionalGetWithoutIsPresent
        assertNotEquals(oldConcept, expenseAfter.get().getConcept());
    }

    @Test
    void givenExpenses_whenDelete_thenStatus200_andShouldDeleteIt() throws Exception {
        int expensesBefore = expenseRepository.findAll().size();
        long idToDelete = expenseRepository.findAll().get(0).getId();

        mvc.perform(delete("/expenses/" + idToDelete)
                .header(HEADER_AUTHORIZATION_KEY, token))
                .andExpect(status().isOk());

        int expensesAfter = expenseRepository.findAll().size();
        assertEquals(expensesBefore, expensesAfter + 1);
    }

    private void prepareDatabase() {
        Category c = new Category("Name", "Description");
        c.setOwner(monexUserRepository.findAll().get(0));
        categoryRepository.save(c);

        Expense e = new Expense(BigDecimal.valueOf(100.00F), new Date(), "Concept", c);
        e.setOwner(monexUserRepository.findAll().get(0));
        expenseRepository.save(e);
    }
}