package com.urbanojvr.mon3x.authorization;

import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class CategoryAuthorization {

    private final CategoryService categoryService;

    @Autowired
    public CategoryAuthorization(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    public Page<Category> getAll(Pageable pageable) {
        return categoryService.getAll(pageable);
    }

    public Category getById(long id) throws MonexUnauthorizedException, MonexNotFoundException {
        Category category = categoryService.getById(id);

        if (!categoryService.loggedUserIsOwner(id)) {
            throw new MonexUnauthorizedException("Category forbidden");
        }

        return category;
    }

    public Category save(CategoryDTO categoryDTO) {
        return categoryService.save(categoryDTO);
    }

    public Category update(long categoryId, CategoryDTO categoryDTO) throws MonexNotFoundException, MonexUnauthorizedException {
        if (!categoryService.loggedUserIsOwner(categoryId)) {
            throw new MonexUnauthorizedException("Category forbidden");
        }

        return categoryService.update(categoryId, categoryDTO);
    }

    public void delete(long categoryId) throws MonexUnauthorizedException {
        if (!categoryService.loggedUserIsOwner(categoryId)) {
            throw new MonexUnauthorizedException("Category forbidden");
        }

        categoryService.delete(categoryId);
    }
}
