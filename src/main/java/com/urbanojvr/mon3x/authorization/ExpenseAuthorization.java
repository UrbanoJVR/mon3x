package com.urbanojvr.mon3x.authorization;

import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import com.urbanojvr.mon3x.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class ExpenseAuthorization {

    private final ExpenseService expenseService;

    private final CategoryService categoryService;

    @Autowired
    public ExpenseAuthorization(ExpenseService expenseService, CategoryService categoryService) {
        this.expenseService = expenseService;
        this.categoryService = categoryService;
    }

    public Page<Expense> getAll(Pageable pageable) {
        return expenseService.getAll(pageable);
    }

    public Expense save(ExpenseDTO dto) throws MonexUnauthorizedException, MonexNotFoundException {
        if (!verifyCategoryOwner(dto.getCategory().getId())) {
            throw new MonexUnauthorizedException("Category forbidden");
        }

        return expenseService.save(dto);
    }

    public void delete(long expenseId) throws MonexUnauthorizedException {
        if (!expenseService.loggedUserIsOwner(expenseId)) {
            throw new MonexUnauthorizedException("Expense forbidden");
        }
        expenseService.delete(expenseId);
    }

    public Expense update(long expenseId, ExpenseDTO dto) throws MonexNotFoundException, MonexUnauthorizedException {
        if (!expenseService.loggedUserIsOwner(expenseId)) {
            throw new MonexUnauthorizedException("Expense forbidden");
        }

        if (!expenseService.loggedUserIsOwner(dto.getCategory().getId())) {
            throw new MonexUnauthorizedException("Category forbidden");
        }

        return expenseService.update(expenseId, dto);
    }

    public boolean verifyCategoryOwner(long categoryId) throws MonexNotFoundException {
        Category category = categoryService.getById(categoryId);

        return category.getOwner().getId().equals(expenseService.getLoggedUserId());
    }
}
