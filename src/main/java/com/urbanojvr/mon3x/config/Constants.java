package com.urbanojvr.mon3x.config;

public class Constants {

    public static final String POPULATE_DATABASE_URL = "/database/populate";

    public static final String LOGIN_URL = "/login";
    public static final String HEADER_AUTHORIZATION_KEY = "Authorization";
    public static final String TOKEN_BEARER_PREFIX = "Bearer ";

    public static final String ISSUER_INFO = "UrbanoJVR";
    public static final String SUPER_SECRET_KEY = "1234";
    public static final long TOKEN_EXPIRATION_TIME = 864_000_000; // 10 day

    // MESSAGES
    public static final String MESSAGE = "Message: ";

    public static final String SAVE_SUCCESS = "Object saved successfully";
    public static final String DELETE_SUCCESS = "Object deleted successfully";
    public static final String UPDATE_SUCCESS = "Object deleted successfully";

    public static final String CATEGORY_NOT_FOUND = "Requested category not found";
    public static final String CATEGORY_FORBIDDEN = "Requested category is forbidden";

    public static final String EXPENSE_NOT_FOUND = "Requested expense not found";
    public static final String EXPENSE_FORBIDDEN = "Requested expense forbidden";

    public static final String INTERNAL_ERROR = "Internal server error ";

}
