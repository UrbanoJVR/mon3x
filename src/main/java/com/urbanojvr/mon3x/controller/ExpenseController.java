package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.authorization.ExpenseAuthorization;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.ExpenseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.urbanojvr.mon3x.config.Constants.*;

@RestController
@RequestMapping(value = "/expenses")
public class ExpenseController {

    private final ExpenseService service;

    private final ExpenseAuthorization expenseAuthorization;

    @Autowired
    public ExpenseController(ExpenseService expenseService, ExpenseAuthorization expenseAuthorization) {
        this.service = expenseService;
        this.expenseAuthorization = expenseAuthorization;
    }

    @GetMapping
    @ResponseBody
    public Page<Expense> getAll(Pageable pageable) {
        return expenseAuthorization.getAll(pageable);
    }

    @GetMapping("/{year}/{month}")
    @ResponseBody
    public ResponseEntity<?> getByYearAndMonth(@PathVariable int year, @PathVariable int month) {
        try {
            List<Expense> expenses = service.getByYearAndMonth(year, month);
            return new ResponseEntity<>(expenses, HttpStatus.OK);
        } catch (Exception e) {
            return generateResponseEntity(INTERNAL_ERROR + e.getMessage() + e.getCause(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public Expense save(@RequestBody ExpenseDTO dto) throws MonexUnauthorizedException, MonexNotFoundException {
            return expenseAuthorization.save(dto);
    }

    @PutMapping(value = "{expenseId}")
    public Expense update(@PathVariable("expenseId") long expenseId, @RequestBody ExpenseDTO expenseDTO) throws MonexNotFoundException, MonexUnauthorizedException {
            return expenseAuthorization.update(expenseId, expenseDTO);
    }

    @DeleteMapping(value = "/{expenseId}")
    public void delete(@PathVariable("expenseId") long expenseId) throws MonexUnauthorizedException {
        expenseAuthorization.delete(expenseId);
    }

    private ResponseEntity<?> generateResponseEntity(String message, HttpStatus status) {
        Map<String, Object> response = new HashMap<>();
        response.put("Message", message);
        return new ResponseEntity<>(response, status);
    }
}
