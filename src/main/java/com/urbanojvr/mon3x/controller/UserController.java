package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.service.MonexUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private MonexUserService service;

    @Autowired
    public UserController(MonexUserService service) {
        this.service = service;
    }

    @PostMapping
    public MonexUser saveUser(@RequestBody MonexUser monexUser) {
        return service.save(monexUser);
    }
}
