package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.authorization.CategoryAuthorization;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import com.urbanojvr.mon3x.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

import static com.urbanojvr.mon3x.config.Constants.*;

@RestController
@RequestMapping(value = "/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final CategoryAuthorization categoryAuthorization;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryAuthorization categoryAuthorization) {
        this.categoryService = categoryService;
        this.categoryAuthorization = categoryAuthorization;
    }

    @GetMapping
    @ResponseBody
    public Page<Category> getAll(Pageable pageable) {
        return categoryAuthorization.getAll(pageable);
    }

    @GetMapping(value = "/{categoryId}")
    public Category getById(@PathVariable("categoryId") long categoryId) throws MonexUnauthorizedException, MonexNotFoundException {
        return categoryAuthorization.getById(categoryId);
    }

    @PostMapping
    public Category save(@RequestBody CategoryDTO categoryDTO) {
            return categoryAuthorization.save(categoryDTO);
    }

    @PutMapping(value = "/{categoryId}")
    public Category update(@PathVariable("categoryId") long categoryId, @RequestBody CategoryDTO categoryDTO) throws MonexNotFoundException, MonexUnauthorizedException {
        return categoryAuthorization.update(categoryId, categoryDTO);
    }

    @DeleteMapping(value = "/{categoryId}")
    public void delete(@PathVariable("categoryId") long categoryId) throws MonexUnauthorizedException {
        categoryAuthorization.delete(categoryId);
    }
}
