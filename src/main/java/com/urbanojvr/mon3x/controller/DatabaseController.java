package com.urbanojvr.mon3x.controller;

import com.urbanojvr.mon3x.service.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/database")
@Profile({"dev", "heroku"})
public class DatabaseController {

    private final DatabaseService service;

    @Autowired
    public DatabaseController(DatabaseService service) {
        this.service = service;
    }

    @GetMapping("/populate")
    public String populateDatabase() {
        service.populate();
        return "OK";
    }
}
