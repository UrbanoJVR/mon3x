package com.urbanojvr.mon3x.DAO;

import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long> {

    Page<Expense> findAllByOwnerId(Long ownerId, Pageable pageable);

    ArrayList<Expense> findAllByOwnerAndDateGreaterThanEqualAndDateLessThanEqualOrderByDate(MonexUser owner, Date start, Date end);

}
