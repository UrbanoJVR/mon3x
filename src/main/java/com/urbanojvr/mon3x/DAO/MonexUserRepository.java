package com.urbanojvr.mon3x.DAO;

import com.urbanojvr.mon3x.domain.MonexUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonexUserRepository extends JpaRepository<MonexUser, Long> {

    MonexUser findByUsername(String username);
}
