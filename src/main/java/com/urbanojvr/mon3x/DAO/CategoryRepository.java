package com.urbanojvr.mon3x.DAO;

import com.urbanojvr.mon3x.domain.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

    ArrayList<Category> findAll();

    Page<Category> findAllByOwnerId(Long ownerId, Pageable pageable);
}
