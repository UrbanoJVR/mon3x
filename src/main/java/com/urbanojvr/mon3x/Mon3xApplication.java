package com.urbanojvr.mon3x;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mon3xApplication {

    public static void main(String[] args) {
        SpringApplication.run(Mon3xApplication.class, args);
    }

}
