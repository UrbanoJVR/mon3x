package com.urbanojvr.mon3x.domain.DTO;

import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.Expense;

import java.math.BigDecimal;
import java.util.Date;

public class ExpenseDTO {

    private BigDecimal amount;
    private Date date;
    private String concept;
    private Category category;

    public ExpenseDTO(BigDecimal amount, Date date, String concept, Category category) {
        this.amount = amount;
        this.date = date;
        this.concept = concept;
        this.category = category;
    }

    public ExpenseDTO(Expense expense) {
        this.amount = expense.getAmount();
        this.date = expense.getDate();
        this.concept = expense.getConcept();
        this.category = expense.getCategory();
    }

    public Expense toExpense() {
        return new Expense(this.amount, this.date, this.concept, this.category);
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
