package com.urbanojvr.mon3x.domain;

import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Category implements BaseObject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    private String name;

    private String description;

    @ManyToOne
    @NotNull
    private MonexUser owner;

    protected Category() {
    }

    public Category(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Category(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public CategoryDTO toCategoryDTO() {
        return new CategoryDTO(this.name, this.description);
    }

    public void updateFromDTO(CategoryDTO dto) {
        this.name = dto.getName();
        this.description = dto.getDescription();
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public MonexUser getOwner() {
        return owner;
    }

    @Override
    public void setOwner(MonexUser owner) {
        this.owner = owner;
    }
}
