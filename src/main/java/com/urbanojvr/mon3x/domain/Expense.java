package com.urbanojvr.mon3x.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class Expense implements BaseObject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(scale = 2)
    private @NotNull @Digits(integer = 9, fraction = 2) BigDecimal amount;

    @NotNull
    @JsonFormat(pattern = "dd-MM-yyyy")
    @Temporal(TemporalType.DATE)
    private Date date;

    @NotNull
    private String concept;

    @ManyToOne
    @NotNull
    private Category category;

    @ManyToOne
    @NotNull
    private MonexUser owner;

    public Expense() {
    }

    public Expense(BigDecimal amount, Date date, String concept, Category category) {
        this.amount = amount;
        this.date = date;
        this.concept = concept;
        this.category = category;
    }

    public Expense(long id, BigDecimal amount, Date date, String concept, Category category) {
        this.id = id;
        this.amount = amount;
        this.date = date;
        this.concept = concept;
        this.category = category;
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public MonexUser getOwner() {
        return owner;
    }

    public void setOwner(MonexUser owner) {
        this.owner = owner;
    }

    public void updateFromDTO(ExpenseDTO dto) {
        this.amount = dto.getAmount();
        this.date = dto.getDate();
        this.concept = dto.getConcept();
        this.category = dto.getCategory();
    }

    public ExpenseDTO toDto() {
        return new ExpenseDTO(this);
    }
}
