package com.urbanojvr.mon3x.domain;

public interface BaseObject {

    long getId();

    MonexUser getOwner();

    void setOwner(MonexUser owner);
}
