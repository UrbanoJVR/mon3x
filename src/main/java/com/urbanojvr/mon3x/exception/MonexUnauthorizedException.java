package com.urbanojvr.mon3x.exception;

public class MonexUnauthorizedException extends Exception{

    public MonexUnauthorizedException() {
        super();
    }

    public MonexUnauthorizedException(String message) {
        super(message);
    }

    public MonexUnauthorizedException(String message, Throwable cause) {
        super(message, cause);
    }
}
