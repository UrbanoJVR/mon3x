package com.urbanojvr.mon3x.exception;

public class MonexNotFoundException extends Exception{

    public MonexNotFoundException() {
        super();
    }

    public MonexNotFoundException(String message) {
        super(message);
    }

    public MonexNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
