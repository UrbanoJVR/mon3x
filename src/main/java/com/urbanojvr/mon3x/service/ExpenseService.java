package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.BaseObject;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.ExpenseDTO;
import com.urbanojvr.mon3x.domain.Expense;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import com.urbanojvr.mon3x.exception.MonexUnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

@Service
public class ExpenseService extends BaseService {

    private final ExpenseRepository expenseRepository;

    @Autowired
    public ExpenseService(MonexUserRepository monexUserRepository, ExpenseRepository expenseRepository) {
        super(monexUserRepository);
        this.expenseRepository = expenseRepository;
    }

    public Page<Expense> getAll(Pageable pageable) {
        return expenseRepository.findAllByOwnerId(getLoggedUserId(), pageable);
    }

    public Expense getById(long id) throws MonexNotFoundException {
        return expenseRepository.findById(id).orElseThrow(() -> new MonexNotFoundException("Expense not exists"));
    }

    public Expense save(ExpenseDTO dto) {
        Expense expense = new Expense();
        expense.updateFromDTO(dto);
        setOwner(expense);

         return expenseRepository.save(expense);
    }

    public Expense update(long expenseId, ExpenseDTO dto) throws MonexNotFoundException {
        Expense expenseToUpdate = this.getById(expenseId);
        expenseToUpdate.updateFromDTO(dto);

        return expenseRepository.save(expenseToUpdate);
    }

    public void delete(long expenseId) {
        expenseRepository.deleteById(expenseId);
    }

    public List<Expense> getByYearAndMonth(int year, int month) {
        Calendar start = new GregorianCalendar(year, month, 1);
        Calendar end = new GregorianCalendar(year, month, 31);

        return expenseRepository.
                findAllByOwnerAndDateGreaterThanEqualAndDateLessThanEqualOrderByDate(getLoggedUser(),
                        start.getTime(), end.getTime());
    }

    @Override
    public boolean exists(BaseObject objToCheck) {
        return expenseRepository.existsById(objToCheck.getId());
    }

    @Override
    public boolean exists(long id) {
        return expenseRepository.existsById(id);
    }

}
