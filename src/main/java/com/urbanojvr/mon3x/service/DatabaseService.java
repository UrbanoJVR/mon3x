package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.ExpenseRepository;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.Expense;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Random;

@Service
public class DatabaseService {

    private int categoriesToInsert;
    private int expensesToInsert;

    private final CategoryRepository categoryRepository;

    private final ExpenseRepository expenseRepository;

    @Autowired
    public DatabaseService(CategoryRepository categoryRepository, ExpenseRepository expenseRepository) {
        this.categoryRepository = categoryRepository;
        this.expenseRepository = expenseRepository;
        categoriesToInsert = 10;
        expensesToInsert = 100;
    }

    public void populate() {
        expenseRepository.deleteAll();
        categoryRepository.deleteAll();
        insertCategories();
        insertExpenses();
    }

    private void insertCategories() {
        for (int i = 0; i < categoriesToInsert; i++) {
            categoryRepository.save(new Category("Name " + i, "Description " + i));
        }
    }

    private void insertExpenses() {
        for (int i = 0; i < expensesToInsert; i++) {

            Category c = categoryRepository.findAll().get(0);

            Expense e = new Expense(randomAmount(), new Date(), "Concept " + i, c);

            expenseRepository.save(e);
        }
    }

    private BigDecimal randomAmount() {
        Random r = new Random();
        int decimalPart = r.nextInt(99);

        int integerPart = r.nextInt(10) + 1;

        String decimalPartString;

        if (decimalPart < 10) {
            decimalPartString = "0" + decimalPart;
        } else {
            decimalPartString = String.valueOf(decimalPart);
        }

        String s = integerPart + "." + decimalPartString;

        return new BigDecimal(s);
    }

    public int getCategoriesToInsert() {
        return categoriesToInsert;
    }

    public void setCategoriesToInsert(int categoriesToInsert) {
        this.categoriesToInsert = categoriesToInsert;
    }

    public int getExpensesToInsert() {
        return expensesToInsert;
    }

    public void setExpensesToInsert(int expensesToInsert) {
        this.expensesToInsert = expensesToInsert;
    }
}
