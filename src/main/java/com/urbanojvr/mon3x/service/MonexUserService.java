package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.MonexUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Qualifier("MonexUserService")
public class MonexUserService implements UserDetailsService {

    private MonexUserRepository monexUserRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public MonexUserService(MonexUserRepository monexUserRepository, PasswordEncoder passwordEncoder) {
        this.monexUserRepository = monexUserRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public MonexUser save(MonexUser monexUser) {
        monexUser.setPassword(passwordEncoder.encode(monexUser.getPassword()));
        return monexUserRepository.save(monexUser);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        MonexUser user = monexUserRepository.findByUsername(userName);

        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException("User " + userName + " not found");
        }
    }
}
