package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.CategoryRepository;
import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.BaseObject;
import com.urbanojvr.mon3x.domain.Category;
import com.urbanojvr.mon3x.domain.DTO.CategoryDTO;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService extends BaseService {

    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(MonexUserRepository monexUserRepository, CategoryRepository categoryRepository) {
        super(monexUserRepository);
        this.categoryRepository = categoryRepository;
    }

    public Page<Category> getAll(Pageable pageable) {
        return categoryRepository.findAllByOwnerId(getLoggedUserId(), pageable);
    }

    public Category getById(long id) throws MonexNotFoundException {
        return categoryRepository.findById(id).orElseThrow(() -> new MonexNotFoundException("Category not exists"));
    }

    public Category save(CategoryDTO categoryDTO) {
        Category categoryToSave = categoryDTO.toCategory();
        setOwner(categoryToSave);
        return categoryRepository.save(categoryToSave);
    }

    public Category update(long categoryId, CategoryDTO categoryDto) throws MonexNotFoundException {
        Category categoryToUpdate = this.getById(categoryId);

        categoryToUpdate.setName(categoryDto.getName());
        categoryToUpdate.setDescription(categoryDto.getDescription());

        return categoryRepository.save(categoryToUpdate);
    }

    public void delete(long categoryId) {
        categoryRepository.deleteById(categoryId);
    }

    @Override
    public boolean exists(BaseObject objToCheck) {
        return categoryRepository.existsById(objToCheck.getId());
    }

    @Override
    public boolean exists(long id) {
        return categoryRepository.existsById(id);
    }

}
