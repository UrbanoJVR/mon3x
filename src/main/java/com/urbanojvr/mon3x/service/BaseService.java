package com.urbanojvr.mon3x.service;

import com.urbanojvr.mon3x.DAO.MonexUserRepository;
import com.urbanojvr.mon3x.domain.BaseObject;
import com.urbanojvr.mon3x.domain.MonexUser;
import com.urbanojvr.mon3x.exception.MonexNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public abstract class BaseService {

    private MonexUserRepository monexUserRepository;

    @Autowired
    public BaseService(MonexUserRepository monexUserRepository) {
        this.monexUserRepository = monexUserRepository;
    }

    public void setOwner(BaseObject obj) {
        obj.setOwner(getLoggedUser());
    }

    public Long getLoggedUserId() {
        return getLoggedUser().getId();
    }

    public MonexUser getLoggedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return monexUserRepository.findByUsername(authentication.getName());
    }

    public abstract boolean exists(BaseObject objToCheck);

    public abstract boolean exists(long id);

    public boolean loggedUserIsOwner(BaseObject objToCheck) {
        return getLoggedUserId().equals(objToCheck.getId());
    }

    public boolean loggedUserIsOwner(long id) {
        return getLoggedUserId().equals(id);
    }
}
